/*eslint no-console: 0*/
"use strict";

var http = require("http");
var port = process.env.PORT || 3000;

var getAge = require('get-age');
 
http.createServer(function (req, res) {
  res.writeHead(200, {"Content-Type": "text/plain"});
  res.end("Remote Get-Age for '1990-01-09' is " + getAge('1990-01-09') + "\n");
}).listen(port);

console.log("Server listening on port %d", port);
